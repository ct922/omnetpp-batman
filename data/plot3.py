#%%
#Variable reset
%reset -f

#Import relevant libaries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statistics

def parse_if_number(s):
    try: return float(s)
    except: return True if s=="true" else False if s=="false" else s if s else None

def parse_ndarray(s):
    return np.fromstring(s, sep=' ') if s else None

#%% Load dataset
target1 = "mobility/mobilitynew1.csv"
target7 = "mobility/mobilitynew7.csv"
target13 = "mobility/mobilitynew13.csv"
UDP_sim_time = 3440
control_sim_time = 3540

chunksize = 10 ** 4

#111111111111111111111111111111111111111111111
para_name1 = []
para_N1 = []
send_UDP_count1 = pd.DataFrame()
rcvd_UDP_count1 = pd.DataFrame()
rcvdPkLifetime_vec1 = pd.DataFrame()
send_UDP_sum1 = pd.DataFrame()
forward_UDP_sum1 = pd.DataFrame()
send_ELP_sum1 = pd.DataFrame()
send_ELPProbes_sum1 = pd.DataFrame()
send_OGM_sum1 = pd.DataFrame()

for chunk in pd.read_csv(target1, usecols=['run', 'type', 'module','attrname','attrvalue', 'name', 'value','vectime','vecvalue'],converters = {'attrvalue': parse_if_number,'vectime': parse_ndarray,'vecvalue': parse_ndarray}, chunksize=chunksize):
    para1 = chunk[(chunk.attrname=='N')]  # filter rows
    index = para1.index
    for i in range(len(para1)):
        j = index[i]
        para_name1.append(para1.run[j])
        para_N1.append(int(para1.attrvalue[j]))
    send_UDP_count1 = send_UDP_count1.append(chunk[(chunk.name=='sentPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvd_UDP_count1 = rcvd_UDP_count1.append(chunk[(chunk.name=='rcvdPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvdPkLifetime_vec1 = rcvdPkLifetime_vec1.append(chunk[(chunk.name=='rcvdPkLifetime:vector')]) 
    send_UDP_sum1 = send_UDP_sum1.append(chunk[(chunk.name=='sendUDP:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    forward_UDP_sum1 = forward_UDP_sum1.append(chunk[(chunk.name=='ForwardedUnicasts:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_ELP_sum1  = send_ELP_sum1.append(chunk[(chunk.name=='sendELP:sum(packetBytes)') & (chunk.type=='scalar')])# filter rows
    send_ELPProbes_sum1  = send_ELPProbes_sum1.append(chunk[(chunk.name=='sendELPProbes:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_OGM_sum1  = send_OGM_sum1.append(chunk[(chunk.name=='sendOGMv2:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows

para_zip = zip(para_N1,para_name1)
sorted_para1 = sorted(para_zip)
tuples = zip(*sorted_para1)
sorted_N1, sorted_name1 = [ list(tuple) for tuple in  tuples]

#777777777777777777777777777777777777777777777
para_name7 = []
para_N7 = []
send_UDP_count7 = pd.DataFrame()
rcvd_UDP_count7 = pd.DataFrame()
rcvdPkLifetime_vec7 = pd.DataFrame()
send_UDP_sum7 = pd.DataFrame()
forward_UDP_sum7 = pd.DataFrame()
send_ELP_sum7 = pd.DataFrame()
send_ELPProbes_sum7 = pd.DataFrame()
send_OGM_sum7 = pd.DataFrame()

for chunk in pd.read_csv(target7, usecols=['run', 'type', 'module','attrname','attrvalue', 'name', 'value','vectime','vecvalue'],converters = {'attrvalue': parse_if_number,'vectime': parse_ndarray,'vecvalue': parse_ndarray}, chunksize=chunksize):
    para7 = chunk[(chunk.attrname=='N')]  # filter rows
    index = para7.index
    for i in range(len(para7)):
        j = index[i]
        para_name7.append(para7.run[j])
        para_N7.append(int(para7.attrvalue[j]))
    send_UDP_count7 = send_UDP_count7.append(chunk[(chunk.name=='sentPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvd_UDP_count7 = rcvd_UDP_count7.append(chunk[(chunk.name=='rcvdPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvdPkLifetime_vec7 = rcvdPkLifetime_vec7.append(chunk[(chunk.name=='rcvdPkLifetime:vector')]) 
    send_UDP_sum7 = send_UDP_sum7.append(chunk[(chunk.name=='sendUDP:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    forward_UDP_sum7 = forward_UDP_sum7.append(chunk[(chunk.name=='ForwardedUnicasts:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_ELP_sum7  = send_ELP_sum7.append(chunk[(chunk.name=='sendELP:sum(packetBytes)') & (chunk.type=='scalar')])# filter rows
    send_ELPProbes_sum7  = send_ELPProbes_sum7.append(chunk[(chunk.name=='sendELPProbes:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_OGM_sum7  = send_OGM_sum7.append(chunk[(chunk.name=='sendOGMv2:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows

para_zip = zip(para_N7,para_name7)
sorted_para7 = sorted(para_zip)
tuples = zip(*sorted_para7)
sorted_N7, sorted_name7 = [ list(tuple) for tuple in  tuples]

#131313131313131313131313131313131313131313131313131
para_name13 = []
para_N13 = []
send_UDP_count13 = pd.DataFrame()
rcvd_UDP_count13 = pd.DataFrame()
rcvdPkLifetime_vec13 = pd.DataFrame()
send_UDP_sum13 = pd.DataFrame()
forward_UDP_sum13 = pd.DataFrame()
send_ELP_sum13 = pd.DataFrame()
send_ELPProbes_sum13 = pd.DataFrame()
send_OGM_sum13 = pd.DataFrame()

for chunk in pd.read_csv(target13, usecols=['run', 'type', 'module','attrname','attrvalue', 'name', 'value','vectime','vecvalue'],converters = {'attrvalue': parse_if_number,'vectime': parse_ndarray,'vecvalue': parse_ndarray}, chunksize=chunksize):
    para13 = chunk[(chunk.attrname=='N')]  # filter rows
    index = para13.index
    for i in range(len(para13)):
        j = index[i]
        para_name13.append(para13.run[j])
        para_N13.append(int(para13.attrvalue[j]))
    send_UDP_count13 = send_UDP_count13.append(chunk[(chunk.name=='sentPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvd_UDP_count13 = rcvd_UDP_count13.append(chunk[(chunk.name=='rcvdPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvdPkLifetime_vec13 = rcvdPkLifetime_vec13.append(chunk[(chunk.name=='rcvdPkLifetime:vector')]) 
    send_UDP_sum13 = send_UDP_sum13.append(chunk[(chunk.name=='sendUDP:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    forward_UDP_sum13 = forward_UDP_sum13.append(chunk[(chunk.name=='ForwardedUnicasts:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_ELP_sum13  = send_ELP_sum13.append(chunk[(chunk.name=='sendELP:sum(packetBytes)') & (chunk.type=='scalar')])# filter rows
    send_ELPProbes_sum13  = send_ELPProbes_sum13.append(chunk[(chunk.name=='sendELPProbes:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_OGM_sum13  = send_OGM_sum13.append(chunk[(chunk.name=='sendOGMv2:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows

para_zip = zip(para_N13,para_name13)
sorted_para13 = sorted(para_zip)
tuples = zip(*sorted_para13)
sorted_N13, sorted_name13 = [ list(tuple) for tuple in  tuples]

# %% plots

fig_plr, ax_plr = plt.subplots(figsize=(8, 4))
fig_over, ax_over = plt.subplots(figsize=(8, 4))
fig_UDP, ax_UDP = plt.subplots(figsize=(8, 4))
fig_ELP, ax_ELP = plt.subplots(figsize=(8, 4))
fig_probe, ax_probe = plt.subplots(figsize=(8, 4))
fig_OGM, ax_OGM = plt.subplots(figsize=(8, 4))
fig_delay, ax_delay = plt.subplots(figsize=(8, 4))
names1 = "1.5 m/s"
names7 = "7 m/s"
names13 = "13 m/s"
fig_name = "mob_new"



plr1 = [0]*len(sorted_name1)
plr7 = [0]*len(sorted_name1)
plr13 = [0]*len(sorted_name1)
for i in range(len(sorted_name1)):
    tmp_send = send_UDP_count1[(send_UDP_count1.run==sorted_name1[i])]
    tmp_rcvd = rcvd_UDP_count1[(rcvd_UDP_count1.run==sorted_name1[i])]
    cum_send = (sum(tmp_send.value))
    cum_rcvd = (sum(tmp_rcvd.value))
    plr1[i] = 1-(cum_rcvd/cum_send)

    tmp_send = send_UDP_count7[(send_UDP_count7.run==sorted_name7[i])]
    tmp_rcvd = rcvd_UDP_count7[(rcvd_UDP_count7.run==sorted_name7[i])]
    cum_send = (sum(tmp_send.value))
    cum_rcvd = (sum(tmp_rcvd.value))
    plr7[i] = 1-(cum_rcvd/cum_send)

    tmp_send = send_UDP_count13[(send_UDP_count13.run==sorted_name13[i])]
    tmp_rcvd = rcvd_UDP_count13[(rcvd_UDP_count13.run==sorted_name13[i])]
    cum_send = (sum(tmp_send.value))
    cum_rcvd = (sum(tmp_rcvd.value))
    plr13[i] = 1-(cum_rcvd/cum_send)

mean_plr1 = [0]*5
lower1 = [0]*5
upper1 = [0]*5
mean_plr7 = [0]*5
lower7 = [0]*5
upper7 = [0]*5
mean_plr13 = [0]*5
lower13 = [0]*5
upper13 = [0]*5
plot_n = [0]*5
for i in range(0,50,10):
    mean_plr1[int(i/10)] = statistics.mean(plr1[i:i+10])
    var = statistics.stdev(plr1[i:i+10])
    upper1[int(i/10)] = mean_plr1[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower1[int(i/10)] = mean_plr1[int(i/10)] - 1.96 * (var/np.sqrt(10))
    
    mean_plr7[int(i/10)] = statistics.mean(plr7[i:i+10])
    var = statistics.stdev(plr7[i:i+10])
    upper7[int(i/10)] = mean_plr7[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower7[int(i/10)] = mean_plr7[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_plr13[int(i/10)] = statistics.mean(plr13[i:i+10])
    var = statistics.stdev(plr13[i:i+10])
    upper13[int(i/10)] = mean_plr13[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower13[int(i/10)] = mean_plr13[int(i/10)] - 1.96 * (var/np.sqrt(10))
    plot_n[int(i/10)] = sorted_N1[i]

ax_plr.plot(plot_n, mean_plr1, marker='o', label=names1+" mean PLR")
ax_plr.fill_between(plot_n, lower1, upper1, color='b', alpha=.1,label=names1+" 95% confidence interval")
ax_plr.plot(plot_n, mean_plr7, marker='o', label=names7+" mean PLR")
ax_plr.fill_between(plot_n, lower7, upper7, color='orange', alpha=.1,label=names7+" 95% confidence interval")
ax_plr.plot(plot_n, mean_plr13, marker='o', label=names13+" mean PLR")
ax_plr.fill_between(plot_n, lower13, upper13, color='g', alpha=.1,label=names13+" 95% confidence interval")
ax_plr.axis([min(para_N1), max(para_N1), 0, 1])
ax_plr.grid()
box = ax_plr.get_position()
ax_plr.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_plr.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_plr.set_xlabel("Number of nodes")
ax_plr.set_ylabel("Packet loss ratio")
fig_plr.show()
fig_plr.savefig(fig_name+'_plr.pdf')





lifetime1 = [0]*len(sorted_name1)
lifetime7 = [0]*len(sorted_name1)
lifetime13 = [0]*len(sorted_name1)
for i in range(len(sorted_name1)):
    tmp_vec = rcvdPkLifetime_vec1[(rcvdPkLifetime_vec1.run==sorted_name1[i])]  # filter rows
    if not tmp_vec.empty:
        tmp_vec_value = tmp_vec.iloc[0,8]
        lifetime1[i] = np.mean(tmp_vec_value)
    tmp_vec = rcvdPkLifetime_vec7[(rcvdPkLifetime_vec7.run==sorted_name7[i])]  # filter rows
    if not tmp_vec.empty:
        tmp_vec_value = tmp_vec.iloc[0,8]
        lifetime7[i] = np.mean(tmp_vec_value)
    tmp_vec = rcvdPkLifetime_vec13[(rcvdPkLifetime_vec13.run==sorted_name13[i])]  # filter rows
    if not tmp_vec.empty:
        tmp_vec_value = tmp_vec.iloc[0,8]
        lifetime13[i] = np.mean(tmp_vec_value)

mean_lifetime1 = [0]*5
lower1 = [0]*5
upper1 = [0]*5
mean_lifetime7 = [0]*5
lower7 = [0]*5
upper7 = [0]*5
mean_lifetime13 = [0]*5
lower13 = [0]*5
upper13 = [0]*5
plot_n = [0]*5
for i in range(0,50,10):
    mean_lifetime1[int(i/10)] = statistics.mean(lifetime1[i:i+10])
    var = statistics.stdev(lifetime1[i:i+10])
    upper1[int(i/10)] = mean_lifetime1[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower1[int(i/10)] = mean_lifetime1[int(i/10)] - 1.96 * (var/np.sqrt(10))
    mean_lifetime7[int(i/10)] = statistics.mean(lifetime7[i:i+10])
    var = statistics.stdev(lifetime7[i:i+10])
    upper7[int(i/10)] = mean_lifetime7[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower7[int(i/10)] = mean_lifetime7[int(i/10)] - 1.96 * (var/np.sqrt(10))
    mean_lifetime13[int(i/10)] = statistics.mean(lifetime13[i:i+10])
    var = statistics.stdev(lifetime13[i:i+10])
    upper13[int(i/10)] = mean_lifetime13[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower13[int(i/10)] = mean_lifetime13[int(i/10)] - 1.96 * (var/np.sqrt(10))
    plot_n[int(i/10)] = sorted_N1[i]

ax_delay.plot(plot_n, mean_lifetime1, marker='o',label=names1+" mean E2E delay")
ax_delay.fill_between(plot_n, lower1, upper1, color='b', alpha=.1,label=names1+" 95% confidence interval")
ax_delay.plot(plot_n, mean_lifetime7, marker='o',label=names7+" mean E2E delay")
ax_delay.fill_between(plot_n, lower7, upper7, color='orange', alpha=.1,label=names7+" 95% confidence interval")
ax_delay.plot(plot_n, mean_lifetime13, marker='o',label=names13+" mean E2E delay")
ax_delay.fill_between(plot_n, lower13, upper13, color='g', alpha=.1,label=names13+" 95% confidence interval")
ax_delay.axis([min(para_N1), max(para_N1), 0, max(mean_lifetime13)+0.02])
ax_delay.grid()
box = ax_delay.get_position()
ax_delay.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_delay.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_delay.set_xlabel("Number of nodes")
ax_delay.set_ylabel("Seconds")
fig_delay.show()
fig_delay.savefig(fig_name+'_delay.pdf')






UDP_overhead1 = [0]*len(sorted_name1)
ELP_overhead1 = [0]*len(sorted_name1)
ELPProbe_overhead1 = [0]*len(sorted_name1)
OGM_overhead1 = [0]*len(sorted_name1)
UDP_overhead7 = [0]*len(sorted_name1)
ELP_overhead7 = [0]*len(sorted_name1)
ELPProbe_overhead7 = [0]*len(sorted_name1)
OGM_overhead7 = [0]*len(sorted_name1)
UDP_overhead13 = [0]*len(sorted_name1)
ELP_overhead13 = [0]*len(sorted_name1)
ELPProbe_overhead13 = [0]*len(sorted_name1)
OGM_overhead13 = [0]*len(sorted_name1)
for i in range(len(sorted_name1)):
    tmp_forward = forward_UDP_sum1[(forward_UDP_sum1.run==sorted_name1[i])]  # filter rows
    UDP_overhead1[i] = ((sum(tmp_forward.value))/UDP_sim_time)/sorted_N1[i]

    tmp_ELP = send_ELP_sum1[(send_ELP_sum1.run==sorted_name1[i])]  # filter rows
    ELP_overhead1[i] = (sum(tmp_ELP.value)/control_sim_time)/sorted_N1[i]

    tmp_ELPProbe = send_ELPProbes_sum1[(send_ELPProbes_sum1.run==sorted_name1[i])]  # filter rows
    ELPProbe_overhead1[i] = (sum(tmp_ELPProbe.value)/control_sim_time)/sorted_N1[i]

    tmp_OGM = send_OGM_sum1[(send_OGM_sum1.run==sorted_name1[i])]  # filter rows
    OGM_overhead1[i] = (sum(tmp_OGM.value)/control_sim_time)/sorted_N1[i]

    tmp_forward = forward_UDP_sum7[(forward_UDP_sum7.run==sorted_name7[i])]  # filter rows
    UDP_overhead7[i] = ((sum(tmp_forward.value))/UDP_sim_time)/sorted_N7[i]

    tmp_ELP = send_ELP_sum7[(send_ELP_sum7.run==sorted_name7[i])]  # filter rows
    ELP_overhead7[i] = (sum(tmp_ELP.value)/control_sim_time)/sorted_N7[i]

    tmp_ELPProbe = send_ELPProbes_sum7[(send_ELPProbes_sum7.run==sorted_name7[i])]  # filter rows
    ELPProbe_overhead7[i] = (sum(tmp_ELPProbe.value)/control_sim_time)/sorted_N7[i]

    tmp_OGM = send_OGM_sum7[(send_OGM_sum7.run==sorted_name7[i])]  # filter rows
    OGM_overhead7[i] = (sum(tmp_OGM.value)/control_sim_time)/sorted_N7[i]

    tmp_forward = forward_UDP_sum13[(forward_UDP_sum13.run==sorted_name13[i])]  # filter rows
    UDP_overhead13[i] = ((sum(tmp_forward.value))/UDP_sim_time)/sorted_N13[i]

    tmp_ELP = send_ELP_sum13[(send_ELP_sum13.run==sorted_name13[i])]  # filter rows
    ELP_overhead13[i] = (sum(tmp_ELP.value)/control_sim_time)/sorted_N13[i]

    tmp_ELPProbe = send_ELPProbes_sum13[(send_ELPProbes_sum13.run==sorted_name13[i])]  # filter rows
    ELPProbe_overhead13[i] = (sum(tmp_ELPProbe.value)/control_sim_time)/sorted_N13[i]

    tmp_OGM = send_OGM_sum13[(send_OGM_sum13.run==sorted_name13[i])]  # filter rows
    OGM_overhead13[i] = (sum(tmp_OGM.value)/control_sim_time)/sorted_N13[i]

mean_UDP1 = [0]*5
lower_UDP1 = [0]*5
upper_UDP1 = [0]*5
mean_ELP1 = [0]*5
lower_ELP1 = [0]*5
upper_ELP1 = [0]*5
mean_probe1 = [0]*5
lower_probe1 = [0]*5
upper_probe1 = [0]*5
mean_OGM1 = [0]*5
lower_OGM1 = [0]*5
upper_OGM1 = [0]*5

mean_UDP7 = [0]*5
lower_UDP7 = [0]*5
upper_UDP7 = [0]*5
mean_ELP7 = [0]*5
lower_ELP7 = [0]*5
upper_ELP7 = [0]*5
mean_probe7 = [0]*5
lower_probe7 = [0]*5
upper_probe7 = [0]*5
mean_OGM7 = [0]*5
lower_OGM7 = [0]*5
upper_OGM7 = [0]*5

mean_UDP13 = [0]*5
lower_UDP13 = [0]*5
upper_UDP13 = [0]*5
mean_ELP13 = [0]*5
lower_ELP13 = [0]*5
upper_ELP13 = [0]*5
mean_probe13 = [0]*5
lower_probe13 = [0]*5
upper_probe13 = [0]*5
mean_OGM13 = [0]*5
lower_OGM13 = [0]*5
upper_OGM13 = [0]*5

plot_n = [0]*5
for i in range(0,50,10):
    mean_UDP1[int(i/10)] = statistics.mean(UDP_overhead1[i:i+10])
    var = statistics.stdev(UDP_overhead1[i:i+10])
    upper_UDP1[int(i/10)] = mean_UDP1[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_UDP1[int(i/10)] = mean_UDP1[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_ELP1[int(i/10)] = statistics.mean(ELP_overhead1[i:i+10])
    var = statistics.stdev(ELP_overhead1[i:i+10])
    upper_ELP1[int(i/10)] = mean_ELP1[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_ELP1[int(i/10)] = mean_ELP1[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_probe1[int(i/10)] = statistics.mean(ELPProbe_overhead1[i:i+10])
    var = statistics.stdev(ELPProbe_overhead1[i:i+10])
    upper_probe1[int(i/10)] = mean_probe1[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_probe1[int(i/10)] = mean_probe1[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_OGM1[int(i/10)] = statistics.mean(OGM_overhead1[i:i+10])
    var = statistics.stdev(OGM_overhead1[i:i+10])
    upper_OGM1[int(i/10)] = mean_OGM1[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_OGM1[int(i/10)] = mean_OGM1[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_UDP7[int(i/10)] = statistics.mean(UDP_overhead7[i:i+10])
    var = statistics.stdev(UDP_overhead7[i:i+10])
    upper_UDP7[int(i/10)] = mean_UDP7[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_UDP7[int(i/10)] = mean_UDP7[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_ELP7[int(i/10)] = statistics.mean(ELP_overhead7[i:i+10])
    var = statistics.stdev(ELP_overhead7[i:i+10])
    upper_ELP7[int(i/10)] = mean_ELP7[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_ELP7[int(i/10)] = mean_ELP7[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_probe7[int(i/10)] = statistics.mean(ELPProbe_overhead7[i:i+10])
    var = statistics.stdev(ELPProbe_overhead7[i:i+10])
    upper_probe7[int(i/10)] = mean_probe7[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_probe7[int(i/10)] = mean_probe7[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_OGM7[int(i/10)] = statistics.mean(OGM_overhead7[i:i+10])
    var = statistics.stdev(OGM_overhead7[i:i+10])
    upper_OGM7[int(i/10)] = mean_OGM7[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_OGM7[int(i/10)] = mean_OGM7[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_UDP13[int(i/10)] = statistics.mean(UDP_overhead13[i:i+10])
    var = statistics.stdev(UDP_overhead13[i:i+10])
    upper_UDP13[int(i/10)] = mean_UDP13[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_UDP13[int(i/10)] = mean_UDP13[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_ELP13[int(i/10)] = statistics.mean(ELP_overhead13[i:i+10])
    var = statistics.stdev(ELP_overhead13[i:i+10])
    upper_ELP13[int(i/10)] = mean_ELP13[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_ELP13[int(i/10)] = mean_ELP13[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_probe13[int(i/10)] = statistics.mean(ELPProbe_overhead13[i:i+10])
    var = statistics.stdev(ELPProbe_overhead13[i:i+10])
    upper_probe13[int(i/10)] = mean_probe13[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_probe13[int(i/10)] = mean_probe13[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_OGM13[int(i/10)] = statistics.mean(OGM_overhead13[i:i+10])
    var = statistics.stdev(OGM_overhead13[i:i+10])
    upper_OGM13[int(i/10)] = mean_OGM13[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_OGM13[int(i/10)] = mean_OGM13[int(i/10)] - 1.96 * (var/np.sqrt(10))

    plot_n[int(i/10)] = sorted_N1[i]

ax_over.plot(plot_n, mean_UDP13, marker='o', label=names13+" UDP overhead")
ax_over.fill_between(plot_n, lower_UDP13, upper_UDP13, color='b', alpha=.1,label=names13+" UDP 95% confidence interval")
ax_over.plot(plot_n, mean_ELP13, marker='o', label=names13+" ELP overhead")
ax_over.fill_between(plot_n, lower_ELP13, upper_ELP13, color='orange', alpha=.1,label=names13+" ELP 95% confidence interval")
ax_over.plot(plot_n, mean_probe13, marker='o', label=names13+" ELP probe overhead")
ax_over.fill_between(plot_n, lower_probe13, upper_probe13, color='g', alpha=.1,label=names13+" probe 95% confidence interval")
ax_over.plot(plot_n, mean_OGM13, marker='o', label=names13+" OGM overhead")
ax_over.fill_between(plot_n, lower_OGM13, upper_OGM13, color='r', alpha=.1,label=names13+" OGM 95% confidence interval")
ax_over.axis([min(para_N1), max(para_N1), 0, max(mean_probe13)+2000])
ax_over.grid()
box = ax_over.get_position()
ax_over.set_position([box.x0, box.y0 + box.height * 0.3, box.width, box.height * 0.7])
ax_over.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_over.set_xlabel("Number of nodes")
ax_over.set_ylabel("Mean bytes per second per node")
fig_over.show()
fig_over.savefig(fig_name+'_overhead.pdf')

ax_UDP.plot(plot_n, mean_UDP1, marker='o', label=names1+" UDP overhead")
ax_UDP.fill_between(plot_n, lower_UDP1, upper_UDP1, color='b', alpha=.1,label=names1+" UDP 95% confidence interval")
ax_UDP.plot(plot_n, mean_UDP7, marker='o', label=names7+" UDP overhead")
ax_UDP.fill_between(plot_n, lower_UDP7, upper_UDP7, color='orange', alpha=.1,label=names7+" UDP 95% confidence interval")
ax_UDP.plot(plot_n, mean_UDP13, marker='o', label=names13+" UDP overhead")
ax_UDP.fill_between(plot_n, lower_UDP13, upper_UDP13, color='g', alpha=.1,label=names13+" UDP 95% confidence interval")
ax_UDP.axis([min(para_N1), max(para_N1), 0, max(mean_UDP1)+2000])
ax_UDP.grid()
box = ax_UDP.get_position()
ax_UDP.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_UDP.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_UDP.set_xlabel("Number of nodes")
ax_UDP.set_ylabel("Mean bytes per second per node")
fig_UDP.show()
fig_UDP.savefig(fig_name+'_UDP_overhead.pdf')

ax_ELP.plot(plot_n, mean_ELP1, marker='o', label=names1+" ELP overhead")
ax_ELP.fill_between(plot_n, lower_ELP1, upper_ELP1, color='b', alpha=.1,label=names1+" ELP 95% confidence interval")
ax_ELP.plot(plot_n, mean_ELP7, marker='o', label=names7+" ELP overhead")
ax_ELP.fill_between(plot_n, lower_ELP7, upper_ELP7, color='orange', alpha=.1,label=names7+" ELP 95% confidence interval")
ax_ELP.plot(plot_n, mean_ELP13, marker='o', label=names13+" ELP overhead")
ax_ELP.fill_between(plot_n, lower_ELP13, upper_ELP13, color='g', alpha=.1,label=names13+" ELP 95% confidence interval")
ax_ELP.axis([min(para_N1), max(para_N1), 0, max(mean_ELP1)+100])
ax_ELP.grid()
box = ax_ELP.get_position()
ax_ELP.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_ELP.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_ELP.set_xlabel("Number of nodes")
ax_ELP.set_ylabel("Mean bytes per second per node")
fig_ELP.show()
fig_ELP.savefig(fig_name+'_ELP_overhead.pdf')

ax_probe.plot(plot_n, mean_probe1, marker='o', label=names1+" probe overhead")
ax_probe.fill_between(plot_n, lower_probe1, upper_probe1, color='b', alpha=.1,label=names1+" probe 95% confidence interval")
ax_probe.plot(plot_n, mean_probe7, marker='o', label=names7+" probe overhead")
ax_probe.fill_between(plot_n, lower_probe7, upper_probe7, color='orange', alpha=.1,label=names7+" probe 95% confidence interval")
ax_probe.plot(plot_n, mean_probe13, marker='o', label=names13+" probe overhead")
ax_probe.fill_between(plot_n, lower_probe13, upper_probe13, color='g', alpha=.1,label=names13+" probe 95% confidence interval")
ax_probe.axis([min(para_N1), max(para_N1), 0, max(mean_probe13)+500])
ax_probe.grid()
box = ax_probe.get_position()
ax_probe.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_probe.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_probe.set_xlabel("Number of nodes")
ax_probe.set_ylabel("Mean bytes per second per node")
fig_probe.show()
fig_probe.savefig(fig_name+'_probe_overhead.pdf')

ax_OGM.plot(plot_n, mean_OGM1, marker='o', label=names1+" OGM overhead")
ax_OGM.fill_between(plot_n, lower_OGM1, upper_OGM1, color='b', alpha=.1,label=names1+" OGM 95% confidence interval")
ax_OGM.plot(plot_n, mean_OGM7, marker='o', label=names7+" OGM overhead")
ax_OGM.fill_between(plot_n, lower_OGM7, upper_OGM7, color='orange', alpha=.1,label=names7+" OGM 95% confidence interval")
ax_OGM.plot(plot_n, mean_OGM13, marker='o', label=names13+" OGM overhead")
ax_OGM.fill_between(plot_n, lower_OGM13, upper_OGM13, color='g', alpha=.1,label=names13+" OGM 95% confidence interval")
ax_OGM.axis([min(para_N1), max(para_N1), 0, max(mean_OGM1)+100])
ax_OGM.grid()
box = ax_OGM.get_position()
ax_OGM.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_OGM.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_OGM.set_xlabel("Number of nodes")
ax_OGM.set_ylabel("Mean bytes per second per node")
fig_OGM.show()
fig_OGM.savefig(fig_name+'_OGM_overhead.pdf')
# %%
