#%%
#Variable reset
%reset -f

#Import relevant libaries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statistics

def parse_if_number(s):
    try: return float(s)
    except: return True if s=="true" else False if s=="false" else s if s else None

def parse_ndarray(s):
    return np.fromstring(s, sep=' ') if s else None

#%% Load dataset
target = "grid/grid3.csv"

para_name = []
para_N = []
send_UDP_count = pd.DataFrame()
rcvd_UDP_count = pd.DataFrame()
rcvdPkLifetime_vec = pd.DataFrame()
send_UDP_sum = pd.DataFrame()
forward_UDP_sum = pd.DataFrame()
send_ELP_sum = pd.DataFrame()
send_ELPProbes_sum = pd.DataFrame()
send_OGM_sum = pd.DataFrame()

UDP_sim_time = 3440
control_sim_time = 3540

chunksize = 10 ** 4
progress = 0
for chunk in pd.read_csv(target, usecols=['run', 'type', 'module','attrname','attrvalue', 'name', 'value','vectime','vecvalue'],converters = {'attrvalue': parse_if_number,'vectime': parse_ndarray,'vecvalue': parse_ndarray}, chunksize=chunksize):
    print(progress/56)
    progress = progress +1

    para = chunk[(chunk.attrname=='N')]  # filter rows

    index = para.index
    for i in range(len(para)):
        j = index[i]
        para_name.append(para.run[j])
        para_N.append(int(para.attrvalue[j]))

    send_UDP_count = send_UDP_count.append(chunk[(chunk.name=='sentPk:count') & (chunk.type=='scalar')])  # filter rows
    rcvd_UDP_count = rcvd_UDP_count.append(chunk[(chunk.name=='rcvdPk:count') & (chunk.type=='scalar')])  # filter rows

    rcvdPkLifetime_vec = rcvdPkLifetime_vec.append(chunk[(chunk.name=='rcvdPkLifetime:vector')]) 



    send_UDP_sum = send_UDP_sum.append(chunk[(chunk.name=='sendUDP:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    forward_UDP_sum = forward_UDP_sum.append(chunk[(chunk.name=='ForwardedUnicasts:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_ELP_sum  = send_ELP_sum.append(chunk[(chunk.name=='sendELP:sum(packetBytes)') & (chunk.type=='scalar')])# filter rows
    send_ELPProbes_sum  = send_ELPProbes_sum.append(chunk[(chunk.name=='sendELPProbes:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows
    send_OGM_sum  = send_OGM_sum.append(chunk[(chunk.name=='sendOGMv2:sum(packetBytes)') & (chunk.type=='scalar')])  # filter rows


print("Gone through "+ str((10 ** 4)*progress)+" lines!!!")

para_zip = zip(para_N,para_name)
sorted_para = sorted(para_zip)
tuples = zip(*sorted_para)
sorted_N, sorted_name = [ list(tuple) for tuple in  tuples]

# %%

fig_plr, ax_plr = plt.subplots(figsize=(8, 4))
fig_over, ax_over = plt.subplots(figsize=(8, 4))
fig_UDP, ax_UDP = plt.subplots(figsize=(8, 4))
fig_ELP, ax_ELP = plt.subplots(figsize=(8, 4))
fig_probe, ax_probe = plt.subplots(figsize=(8, 4))
fig_OGM, ax_OGM = plt.subplots(figsize=(8, 4))
fig_delay, ax_delay = plt.subplots(figsize=(8, 4))
names = "Grid"
fig_name = "grid_new"



plr = [0]*len(sorted_name)
for i in range(len(sorted_name)):
    tmp_send = send_UDP_count[(send_UDP_count.run==sorted_name[i])]
    tmp_rcvd = rcvd_UDP_count[(rcvd_UDP_count.run==sorted_name[i])]
    cum_send = (sum(tmp_send.value))
    cum_rcvd = (sum(tmp_rcvd.value))
    plr[i] = 1-(cum_rcvd/cum_send)

mean_plr = [0]*9
lower = [0]*9
upper = [0]*9
plot_n = [0]*9
for i in range(0,90,10):
    print(int(i/10))
    mean_plr[int(i/10)] = statistics.mean(plr[i:i+10])
    var = statistics.stdev(plr[i:i+10])
    upper[int(i/10)] = mean_plr[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower[int(i/10)] = mean_plr[int(i/10)] - 1.96 * (var/np.sqrt(10))
    plot_n[int(i/10)] = sorted_N[i]

ax_plr.plot(plot_n, mean_plr, marker='o', label=names+" mean PLR")
ax_plr.fill_between(plot_n, lower, upper, color='b', alpha=.1,label=names+" 95% confidence interval")
ax_plr.axis([min(para_N), max(para_N), 0, 1])
ax_plr.grid()
box = ax_plr.get_position()
ax_plr.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
ax_plr.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_plr.set_xlabel("Number of nodes")
ax_plr.set_ylabel("Packet loss ratio")
fig_plr.show()
fig_plr.savefig(fig_name+'_plr.pdf')





lifetime = [0]*len(sorted_name)
for i in range(len(sorted_name)):
    tmp_vec = rcvdPkLifetime_vec[(rcvdPkLifetime_vec.run==sorted_name[i])]  # filter rows
    if not tmp_vec.empty:
        tmp_vec_value = tmp_vec.iloc[0,8]
        lifetime[i] = np.mean(tmp_vec_value)

mean_lifetime = [0]*9
lower = [0]*9
upper = [0]*9
plot_n = [0]*9
for i in range(0,90,10):
    print(int(i/10))
    mean_lifetime[int(i/10)] = statistics.mean(lifetime[i:i+10])
    var = statistics.stdev(lifetime[i:i+10])
    upper[int(i/10)] = mean_lifetime[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower[int(i/10)] = mean_lifetime[int(i/10)] - 1.96 * (var/np.sqrt(10))
    plot_n[int(i/10)] = sorted_N[i]

ax_delay.plot(plot_n, mean_lifetime, marker='o',label=names+" mean E2E delay")
ax_delay.fill_between(plot_n, lower, upper, color='b', alpha=.1,label=names+" 95% confidence interval")
ax_delay.axis([min(para_N), max(para_N), 0, max(mean_lifetime)+0.02])
ax_delay.grid()
#ax_delay.legend(loc="best")
box = ax_delay.get_position()
ax_delay.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
ax_delay.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_delay.set_xlabel("Number of nodes")
ax_delay.set_ylabel("Seconds")
fig_delay.show()
fig_delay.savefig(fig_name+'_delay.pdf')






UDP_overhead = [0]*len(sorted_name)
ELP_overhead = [0]*len(sorted_name)
ELPProbe_overhead = [0]*len(sorted_name)
OGM_overhead = [0]*len(sorted_name)
for i in range(len(sorted_name)):
    tmp_forward = forward_UDP_sum[(forward_UDP_sum.run==sorted_name[i])]  # filter rows
    UDP_overhead[i] = ((sum(tmp_forward.value))/UDP_sim_time)/sorted_N[i]

    tmp_ELP = send_ELP_sum[(send_ELP_sum.run==sorted_name[i])]  # filter rows
    ELP_overhead[i] = (sum(tmp_ELP.value)/control_sim_time)/sorted_N[i]

    tmp_ELPProbe = send_ELPProbes_sum[(send_ELPProbes_sum.run==sorted_name[i])]  # filter rows
    ELPProbe_overhead[i] = (sum(tmp_ELPProbe.value)/control_sim_time)/sorted_N[i]

    tmp_OGM = send_OGM_sum[(send_OGM_sum.run==sorted_name[i])]  # filter rows
    OGM_overhead[i] = (sum(tmp_OGM.value)/control_sim_time)/sorted_N[i]

mean_UDP = [0]*9
lower_UDP = [0]*9
upper_UDP = [0]*9

mean_ELP = [0]*9
lower_ELP = [0]*9
upper_ELP = [0]*9

mean_probe = [0]*9
lower_probe = [0]*9
upper_probe = [0]*9

mean_OGM = [0]*9
lower_OGM = [0]*9
upper_OGM = [0]*9

plot_n = [0]*9
for i in range(0,90,10):
    print(int(i/10))
    mean_UDP[int(i/10)] = statistics.mean(UDP_overhead[i:i+10])
    var = statistics.stdev(UDP_overhead[i:i+10])
    upper_UDP[int(i/10)] = mean_UDP[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_UDP[int(i/10)] = mean_UDP[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_ELP[int(i/10)] = statistics.mean(ELP_overhead[i:i+10])
    var = statistics.stdev(ELP_overhead[i:i+10])
    upper_ELP[int(i/10)] = mean_ELP[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_ELP[int(i/10)] = mean_ELP[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_probe[int(i/10)] = statistics.mean(ELPProbe_overhead[i:i+10])
    var = statistics.stdev(ELPProbe_overhead[i:i+10])
    upper_probe[int(i/10)] = mean_probe[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_probe[int(i/10)] = mean_probe[int(i/10)] - 1.96 * (var/np.sqrt(10))

    mean_OGM[int(i/10)] = statistics.mean(OGM_overhead[i:i+10])
    var = statistics.stdev(OGM_overhead[i:i+10])
    upper_OGM[int(i/10)] = mean_OGM[int(i/10)] + 1.96 * (var/np.sqrt(10))
    lower_OGM[int(i/10)] = mean_OGM[int(i/10)] - 1.96 * (var/np.sqrt(10))

    plot_n[int(i/10)] = sorted_N[i]

ax_over.plot(plot_n, mean_UDP, marker='o', label=names+" UDP overhead")
ax_over.fill_between(plot_n, lower_UDP, upper_UDP, color='b', alpha=.1,label=names+" UDP 95% confidence interval")
ax_over.plot(plot_n, mean_ELP, marker='o', label=names+" ELP overhead")
ax_over.fill_between(plot_n, lower_ELP, upper_ELP, color='orange', alpha=.1,label=names+" ELP 95% confidence interval")
ax_over.plot(plot_n, mean_probe, marker='o', label=names+" ELP probe overhead")
ax_over.fill_between(plot_n, lower_probe, upper_probe, color='g', alpha=.1,label=names+" probe 95% confidence interval")
ax_over.plot(plot_n, mean_OGM, marker='o', label=names+" OGM overhead")
ax_over.fill_between(plot_n, lower_OGM, upper_OGM, color='r', alpha=.1,label=names+" OGM 95% confidence interval")
ax_over.axis([min(para_N), max(para_N), 0, max(mean_UDP)+2000])
ax_over.grid()
#ax_over.legend(loc="best")
box = ax_over.get_position()
ax_over.set_position([box.x0, box.y0 + box.height * 0.3, box.width, box.height * 0.7])
ax_over.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_over.set_xlabel("Number of nodes")
ax_over.set_ylabel("Mean bytes per second per node")
fig_over.show()
#fig_over.savefig('Probe_overhead.pdf')
fig_over.savefig(fig_name+'_overhead.pdf')

ax_UDP.plot(plot_n, mean_UDP, marker='o', label=names+" UDP overhead")
ax_UDP.fill_between(plot_n, lower_UDP, upper_UDP, color='b', alpha=.1,label=names+" UDP 95% confidence interval")
ax_UDP.axis([min(para_N), max(para_N), 0, max(mean_UDP)+2000])
ax_UDP.grid()
box = ax_UDP.get_position()
ax_UDP.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_UDP.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_UDP.set_xlabel("Number of nodes")
ax_UDP.set_ylabel("Mean bytes per second per node")
fig_UDP.show()
fig_UDP.savefig(fig_name+'_UDP_overhead.pdf')

ax_ELP.plot(plot_n, mean_ELP, marker='o', label=names+" UDP overhead")
ax_ELP.fill_between(plot_n, lower_ELP, upper_ELP, color='b', alpha=.1,label=names+" UDP 95% confidence interval")
ax_ELP.axis([min(para_N), max(para_N), 0, max(mean_ELP)+100])
ax_ELP.grid()
box = ax_ELP.get_position()
ax_ELP.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_ELP.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_ELP.set_xlabel("Number of nodes")
ax_ELP.set_ylabel("Mean bytes per second per node")
fig_ELP.show()
fig_ELP.savefig(fig_name+'_ELP_overhead.pdf')

ax_probe.plot(plot_n, mean_probe, marker='o', label=names+" UDP overhead")
ax_probe.fill_between(plot_n, lower_probe, upper_probe, color='b', alpha=.1,label=names+" UDP 95% confidence interval")
ax_probe.axis([min(para_N), max(para_N), 0, max(mean_probe)+500])
ax_probe.grid()
box = ax_probe.get_position()
ax_probe.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_probe.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_probe.set_xlabel("Number of nodes")
ax_probe.set_ylabel("Mean bytes per second per node")
fig_probe.show()
fig_probe.savefig(fig_name+'_probe_overhead.pdf')

ax_OGM.plot(plot_n, mean_OGM, marker='o', label=names+" UDP overhead")
ax_OGM.fill_between(plot_n, lower_OGM, upper_OGM, color='b', alpha=.1,label=names+" UDP 95% confidence interval")
ax_OGM.axis([min(para_N), max(para_N), 0, max(mean_OGM)+100])
ax_OGM.grid()
box = ax_OGM.get_position()
ax_OGM.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_OGM.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_OGM.set_xlabel("Number of nodes")
ax_OGM.set_ylabel("Mean bytes per second per node")
fig_OGM.show()
fig_OGM.savefig(fig_name+'_OGM_overhead.pdf')
# %%
