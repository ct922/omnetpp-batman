#%%
#Variable reset
%reset -f

#Import relevant libaries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def parse_if_number(s):
    try: return float(s)
    except: return True if s=="true" else False if s=="false" else s if s else None

def parse_ndarray(s):
    return np.fromstring(s, sep=' ') if s else None

# init figs
fig_plr, ax_plr = plt.subplots(figsize=(8, 4))
fig_over, ax_over = plt.subplots(figsize=(8, 4))
fig_delay, ax_delay = plt.subplots(figsize=(8, 4))
fig_conv, ax_conv = plt.subplots(figsize=(8, 4))
iteration = 2

#%% Load dataset
files = ["line/line2","grid/grid2","mobility/mobility1-2","mobility/mobility7-2","mobility/mobility13-2"]
#line = 0
#grid = 1
#mobility = 2, 3 ,4 or set to iteration
target = files[0]+".csv"

data = pd.read_csv(target, usecols=['run', 'type', 'module','attrname','attrvalue', 'name', 'value','vectime','vecvalue'],converters = {'attrvalue': parse_if_number,'vectime': parse_ndarray,'vecvalue': parse_ndarray})
names = "Test"
if target == files[0]+".csv":
    names = "Line"
    fig_name = "line"
if target == files[1]+".csv":
    names = "Grid"
    fig_name = "grid"
if target == files[2]+".csv":
    names = "1.5 m/s"
    fig_name = "mob"
if target == files[3]+".csv":
    names = "7 m/s"
    fig_name = "mob"
if target == files[4]+".csv":
    names = "13 m/s"
    fig_name = "mob"

#print(data.dtypes)
#print(data["name"].unique)

para = data[(data.attrname=='N')]  # filter rows

para_name = [None]*len(para)
para_N = [0]*len(para)
index = para.index
for i in range(len(para)):
    j = index[i]
    para_name[i] = para.run[j]
    para_N[i] = int(para.attrvalue[j])
para_zip = zip(para_N,para_name)
sorted_para = sorted(para_zip)
tuples = zip(*sorted_para)
sorted_N, sorted_name = [ list(tuple) for tuple in  tuples]

#Figure out approximately how long UDP and control packets were sent for
vectors = data[(data.type=='vector')]  # filter rows
UDP_time_vec = vectors[(vectors.name=='sentPk:vector(packetBytes)')]  # filter rows
tmp_index = UDP_time_vec.index
tmp_vec = UDP_time_vec.vectime[tmp_index[0]]
UDP_sim_time = max(tmp_vec)-min(tmp_vec)

control_time_vec = vectors[(vectors.name=='sendELP:vector(packetBytes)')]  # filter rows
tmp_index = control_time_vec.index
tmp_vec = control_time_vec.vectime[tmp_index[0]]
control_sim_time = max(tmp_vec)-min(tmp_vec)
print("done 1")

# %% plr plot

scalars = data[(data.type=='scalar')]  # filter rows
send_UDP_count = scalars[(scalars.name=='sentPk:count')]  # filter rows
rcvd_UDP_count = scalars[(scalars.name=='rcvdPk:count')]  # filter rows

plr = [0]*len(para)
for i in range(len(sorted_name)):
    tmp_send = send_UDP_count[(send_UDP_count.run==sorted_name[i])]
    tmp_rcvd = rcvd_UDP_count[(rcvd_UDP_count.run==sorted_name[i])]
    cum_send = (sum(tmp_send.value))
    cum_rcvd = (sum(tmp_rcvd.value))
    plr[i] = 1-(cum_rcvd/cum_send)

ax_plr.plot(sorted_N, plr, marker='o', label=names+" mean PLR")
ax_plr.axis([min(para_N), max(para_N), 0, 1])
ax_plr.grid()
#ax_plr.legend(loc="best")
box = ax_plr.get_position()
ax_plr.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
ax_plr.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=5)
ax_plr.set_xlabel("Number of nodes")
ax_plr.set_ylabel("Packet loss ratio")
fig_plr.show()
fig_plr.savefig(fig_name+'_plr.pdf')
print("done 2")
# %% overhead plot
scalars = data[(data.type=='scalar')]  # filter rows
send_UDP_sum = scalars[(scalars.name=='sendUDP:sum(packetBytes)')]  # filter rows
forward_UDP_sum = scalars[(scalars.name=='ForwardedUnicasts:sum(packetBytes)')]  # filter rows
send_ELP_sum  = scalars[(scalars.name=='sendELP:sum(packetBytes)')]  # filter rows
send_ELPProbes_sum  = scalars[(scalars.name=='sendELPProbes:sum(packetBytes)')]  # filter rows
send_OGM_sum  = scalars[(scalars.name=='sendOGMv2:sum(packetBytes)')]  # filter rows

UDP_overhead = [0]*len(para)
ELP_overhead = [0]*len(para)
ELPProbe_overhead = [0]*len(para)
OGM_overhead = [0]*len(para)
for i in range(len(para_name)):
    #tmp_UDP = send_UDP_sum[(send_UDP_sum.run==sorted_name[i])]  # filter rows
    tmp_forward = forward_UDP_sum[(forward_UDP_sum.run==sorted_name[i])]  # filter rows
    UDP_overhead[i] = ((sum(tmp_forward.value))/UDP_sim_time)/sorted_N[i]

    tmp_ELP = send_ELP_sum[(send_ELP_sum.run==sorted_name[i])]  # filter rows
    ELP_overhead[i] = (sum(tmp_ELP.value)/control_sim_time)/sorted_N[i]

    tmp_ELPProbe = send_ELPProbes_sum[(send_ELPProbes_sum.run==sorted_name[i])]  # filter rows
    ELPProbe_overhead[i] = (sum(tmp_ELPProbe.value)/control_sim_time)/sorted_N[i]

    tmp_OGM = send_OGM_sum[(send_OGM_sum.run==sorted_name[i])]  # filter rows
    OGM_overhead[i] = (sum(tmp_OGM.value)/control_sim_time)/sorted_N[i]

ax_over.plot(sorted_N, UDP_overhead, marker='o', label=names+" UDP overhead")
ax_over.plot(sorted_N, ELP_overhead, marker='o', label=names+" ELP overhead")
ax_over.plot(sorted_N, ELPProbe_overhead, marker='o', label=names+" ELP probe overhead")
ax_over.plot(sorted_N, OGM_overhead, marker='o', label=names+" OGM overhead")
ax_over.axis([min(para_N), max(para_N), 0, max(ELPProbe_overhead)+2000])
ax_over.grid()
#ax_over.legend(loc="best")
box = ax_over.get_position()
ax_over.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_over.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=2)
ax_over.set_xlabel("Number of nodes")
ax_over.set_ylabel("Mean bytes per second per node")
fig_over.show()
#fig_over.savefig('Probe_overhead.pdf')
fig_over.savefig(fig_name+'_overhead.pdf')
print("done 3")

# %% delay convergence plot

vectors = data[(data.type=='vector')]  # filter rows
rcvdPkLifetime_vec = vectors[(data.name=='rcvdPkLifetime:vector')]  # filter rows

fig, ax = plt.subplots(figsize=(8, 4))

for i in range(len(para_name)):
    tmp_vec = rcvdPkLifetime_vec[(rcvdPkLifetime_vec.run==sorted_name[i])]  # filter rows
    if not tmp_vec.empty:
        tmp_vec_value = tmp_vec.iloc[0,8]
        tmp_vec_time = tmp_vec.iloc[0,7]
        mean = np.zeros(tmp_vec_value.shape[0])
        for j in range(tmp_vec_value.shape[0]):
            mean[j] = np.mean(tmp_vec_value[:j])
        print(mean)
        ax_conv.plot(tmp_vec_time, mean,label=(str(sorted_N[i])+" nodes cumulative delay"))
        
ax_conv.axis([min(tmp_vec_time), max(tmp_vec_time), 0, 1])
ax_conv.grid()
#plt.legend(loc="best")
box = ax.get_position()
ax_conv.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
ax_conv.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=3)
ax_conv.set_xlabel("Elapsed time")
ax_conv.set_ylabel("Seconds")
fig_conv.show()
fig_conv.savefig(fig_name+'conv_delay.pdf')
print("done 4")
# %% delay plot

mean_lifetime = [0]*len(para)
for i in range(len(para_name)):
    tmp_vec = rcvdPkLifetime_vec[(rcvdPkLifetime_vec.run==sorted_name[i])]  # filter rows
    if not tmp_vec.empty:
        tmp_vec_value = tmp_vec.iloc[0,8]
        mean_lifetime[i] = np.mean(tmp_vec_value)

ax_delay.plot(sorted_N, mean_lifetime, marker='o',label=names+" mean E2E delay")
ax_delay.axis([min(para_N), max(para_N), 0, max(mean_lifetime)+0.02])
ax_delay.grid()
#ax_delay.legend(loc="best")
box = ax_delay.get_position()
ax_delay.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
ax_delay.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15),fancybox=True, shadow=False, ncol=5)
ax_delay.set_xlabel("Number of nodes")
ax_delay.set_ylabel("Seconds")
fig_delay.show()
fig_delay.savefig(fig_name+'_delay.pdf')
print("done 5")
# %% iter stuff
iteration = iteration+1
print("done done done")

# %%

# %%

# %%
